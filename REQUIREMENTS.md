<!-- TOC -->

- [General Packages](#general-packages)
- [asdf](#asdf)
- [Docker](#docker)
- [Install Go](#install-go)
- [Helm](#helm)
- [Helm Docs](#helm-docs)
- [Install Kubectl](#install-kubectl)

<!-- TOC -->

# General Packages

Install the follow packages.

Debian/Ubuntu:

```bash
sudo apt-get install -y vim git make curl net-tools acl pkg-config
```

# asdf

> Attention!!!
> To update ``asdf``, ONLY use the following command. If you try to reinstall or update by changing the version in the following commands, it will be necessary to reinstall all plugins/commands installed before, so it is very important to back up the ``$HOME/.asdf`` directory.

```bash
asdf update
```

Run the following commands:

```bash
ASDF_VERSION="v0.14.0"
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch $ASDF_VERSION

# Adicionando no $HOME/.bashrc
echo ". \"\$HOME/.asdf/asdf.sh\"" >> ~/.bashrc
echo ". \"\$HOME/.asdf/completions/asdf.bash\"" >> ~/.bashrc
source ~/.bashrc
```

Reference: https://asdf-vm.com/guide/introduction.html

# Docker

Install Docker following commands.

```bash
curl -fsSL https://get.docker.com -o get-docker.sh;
sudo sh get-docker.sh;
# Using docker without sudo
sudo usermod -aG docker $USER;
sudo setfacl -m user:$USER:rw /var/run/docker.sock
```

Reference: https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot

For more information about Docker Compose visit:

* https://docs.docker.com
* http://blog.aeciopires.com/primeiros-passos-com-docker

# Install Go

Run the following commands to install Go.

```bash
VERSION=1.22.3

mkdir -p $HOME/go/bin

cd /tmp
curl -L https://go.dev/dl/go$VERSION.linux-amd64.tar.gz -o go.tar.gz

sudo rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go.tar.gz
rm /tmp/go.tar.gz

export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin

go version

echo "GOPATH=$HOME/go" >> ~/.bashrc
echo "PATH=\$PATH:/usr/local/go/bin:\$GOPATH/bin" >> ~/.bashrc
```

More informations: https://go.dev/doc/

# Helm

Execute these commands to install helm.

```bash
VERSION="3.15.1"

asdf plugin list all | grep helm
asdf plugin add helm https://github.com/Antiarchitect/asdf-helm.git
asdf latest helm

asdf install helm $VERSION
asdf list helm

# Definindo a versão padrão
asdf global helm $VERSION
asdf list helm
```

# Helm Docs

Run the following commands to install ``helm-docs``.

```bash
VERSION="1.12.0"

asdf plugin list all | grep helm-docs
asdf plugin add helm-docs https://github.com/sudermanjr/asdf-helm-docs.git
asdf latest helm-docs

asdf install helm-docs $VERSION
asdf list helm-docs

# Definindo a versão padrão
asdf global helm-docs $VERSION
asdf list helm-docs
```

Documentation: https://github.com/norwoodj/helm-docs 

The documentation generated by ``helm-docs`` is based on the contents of the ``values.yaml``, ``README.md.gotmpl`` and ``Chart.yaml`` file. It tries to overwrite the contents of the ``README.md`` file within the chart directory.

# Install Kubectl

Simple shell function for Kubectl installation in Linux 64 bits. Copy and paste this code:

```bash
VERSION_OPTION_1="1.30.1"

asdf plugin list all | grep kubectl
asdf plugin add kubectl https://github.com/asdf-community/asdf-kubectl.git
asdf latest kubectl

asdf install kubectl $VERSION_OPTION_1
asdf list kubectl

# Definindo a versão padrão
asdf global kubectl $VERSION_OPTION_1
asdf list kubectl
```

Kubectl documentation:

https://kubernetes.io/docs/reference/kubectl/overview/
