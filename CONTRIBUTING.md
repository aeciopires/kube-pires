<!-- TOC -->

- [Contributing](#contributing)
- [About Visual Code (VSCode)](#about-visual-code-vscode)

<!-- TOC -->

# Contributing

* Install the requirements following the instructions of the file [REQUIREMENTS.md](REQUIREMENTS.md).
* Configure authentication on your account to use the SSH protocol instead of HTTP. See this [tutorial](https://docs.gitlab.com/ee/ssh/)
* Clone this repository to your local system:

```bash
git clone git@gitlab.com:aeciopires/kube-pires.git
```

* Create a branch. Example:

```bash
git checkout -b BRANCH_NAME
```

* Make sure you are on the correct branch using the following command. The branch in use contains the '*' before the name.

```bash
git branch
```

* Make your changes and tests to the new branch.
* Commit the changes to the branch.
* Push files to repository remote with command:

```bash
git push --set-upstream origin BRANCH_NAME;
```

* Create Merge Request (MR) to the `master` branch. See this [tutorial](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
* Update the content with the suggestions of the reviewer (if necessary).
* After your pull request is merged to the `master` branch, update your local clone:

```bash
git checkout master;
git pull;
```

* Clean up after your request is merged with command:

```bash
git branch -d BRANCH_NAME;
```

# About Visual Code (VSCode)

Use a IDE (Integrated Development Environment) or text editor of your choice. By default, the use of VSCode is recommended.

VSCode (https://code.visualstudio.com), combined with the following plugins, helps the editing/review process, mainly allowing the preview of the content before the commit, analyzing the Markdown syntax and generating the automatic summary, as the section titles are created/changed.

Plugins to Visual Code:

* docker: https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker (require docker-ce package)
* gitlens: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens (require git package)
* gotemplate-syntax: https://marketplace.visualstudio.com/items?itemName=casualjim.gotemplate
* Markdown-all-in-one: https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one
* Markdown-lint: https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint
* Markdown-toc: https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc
* YAML: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml
* Helm Intellisense: https://marketplace.visualstudio.com/items?itemName=Tim-Koehler.helm-intellisense

Theme for VSCode:

* https://code.visualstudio.com/docs/getstarted/themes
* https://dev.to/thegeoffstevens/50-vs-code-themes-for-2020-45cc
* https://vscodethemes.com/
