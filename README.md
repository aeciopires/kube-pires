<!-- TOC -->

- [About](#about)
- [Installation](#installation)
  - [Using Docker](#using-docker)
  - [Using Helm](#using-helm)
- [Developers](#developers)
- [License](#license)

<!-- TOC -->

# About

``kube-pires`` is a web application developed using [Golang](https://go.dev).

It's show the follow informations of the pods.

> Thanks [Gabriel Vieira](https://www.linkedin.com/in/gabrielfvieira/) for help with app.

# Installation

## Using Docker

Follow the instructions of file [app/README.md](app/README.md).

## Using Helm

Follow the instructions of the file [helm-chart/README.md](helm-chart/README.md).

# Developers

developer: Aécio dos Santos Pires<br>
mail: http://blog.aeciopires.com/contato

# License

GPL-3.0 2024 Aécio dos Santos Pires
