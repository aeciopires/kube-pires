# Changelog

<!-- TOC -->

- [Changelog](#changelog)
- [v1.0.0](#v100)
- [v0.2.0](#v020)
- [v0.1.0](#v010)

<!-- TOC -->

# v1.0.0

Date: 12/14/2022

* Removed ``health`` endpoint;
* Changed ``gorilla/mux`` framework by ``gin-gonic/gin``;
* Changed default lib of metrics of Prometheus ``penglongli/gin-metrics/ginmetrics``;
* Refactory in code and removed unnecessary code;
* Improvements in documentations and comments in code;

# v0.2.0

Date: 11/23/2022

* Added ``/metrics`` page with prometheus metrics;
* Added function for log of requisitions;

# v0.1.0

Date: 11/22/2022

* Added support to Go 1.19.3;
* Added feautures show hostname and IP Address of network interfaces in ``/`` URL;
* Added ``/headers`` page;
* Added ``/health`` page;
