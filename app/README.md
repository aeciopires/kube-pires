<!-- TOC -->

- [About](#about)
- [Installation](#installation)
  - [Using Docker](#using-docker)
  - [Using Helm](#using-helm)
  - [Compiling with go](#compiling-with-go)

<!-- TOC -->

# About

``kube-pires`` is a web application developed using [Golang](https://go.dev).

It's show the follow informations of the pods:

* ``/``        => hosname, interface name and IP addresses;
* ``/health``  => show status of application;
* ``/metrics`` => show metrics in format supported by prometheus;

# Installation

## Using Docker

Install Docker following the instructions of the file [REQUIREMENTS.md](../REQUIREMENTS.md).

Create a container using ``make`` command:

```bash
make createContainer
```

Open the browser and access the URL: http://localhost:3000

## Using Helm

Follow the instructions of the file [helm-chart/README.md](../helm-chart/README.md).

## Compiling with go

Install Go following the instructions of the file [REQUIREMENTS.md](../REQUIREMENTS.md).

Compile using ``make`` command:

```bash
make build
```
